// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"

enum Errors{INDEX_ERROR; DIF_SIZES};
TBitField::TBitField(int len)
{
	BitLen = len;
	MemLen = len-1/8*sizeof(TELEM)+1;
	pMem = new TELEM [MemLen];
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.bBtLen;
	MemLen = bf.MemLen;
	pMem = new TELEM [MemLen];
}

TBitField::~TBitField()
{
	BitLen = 0;
	MemLen = 0;
	delete[]pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n>BitLen)
		throw INDEX_ERROR;
	return n/(8*sizeof(TELEM));
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if n>sizeof(TELEM)
		throw INDEX_ERROR;
	TELEM mask = 1<<(8*sizeof(TELEM)-(n%(8*sizeof(TELEM)))-1);
	return mask;
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	int index = GetMemIndex(n);
	TELEM mask = GetMemMask(n);
	pMem[index]|=mask;
}

void TBitField::ClrBit(const int n) // очистить бит
{
	int index = GetMemIndex(n);
	TELEM mask = GetMemMask(n);
	pMem[index]&=~mask;
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	int index = GetMemIndex(n);
	TELEM mask = GetMemMask(n);
	TELEM res = pMem[index]%mask;
	if(res)
		return 1;
	else return 0;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (*this == &bf)
		return *this
	if (MemLen!=bf.MemLen)
	{
		for (int i = 0; i< MemLen; i++)
			pMem[i]=0;
		delete[]pMem;
		pMem = new TELEM [bf.MemLen];
	}
	MemLen = bf.MemLen;
	BitLen = bf.BitLen;
	for (int i=0; i<MemLen; i++)
		TELEM[i] = bf.TELEM[i];
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (MemLen!=bf.MemLen)
		return 0 ;
	else
	if (BitLen!=bf.BitLen)
		return 0;
	else
	for (int i=0; i<MemLen;i++)
		if (pMem[i]!=bf.pMem[i])
			return 0;
	return 1;
		
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if (MemLen!=bf.MemLen)
		return 1 ;
	else
	if (BitLen!=bf.BitLen)
		return 1;
	else
	for (int i=0; i<MemLen;i++)
		if (pMem[i]!=bf.pMem[i])
			return 1;
	return 0;
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	if ((MemLen!=bf.MemLen)||(BitLen!=bf.BitLen))
		throw DIF_SIZES;
	TBitField res (BitLen);
	for (int i =0; i<MemLen; i++)
		res.pMem[i]=pMem[i]|bf.pMem[i];
	return res;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	if ((MemLen!=bf.MemLen)||(BitLen!=bf.BitLen))
		throw DIF_SIZES;
	TBitField res (BitLen);
	for (int i =0; i<MemLen; i++)
		res.pMem[i]=pMem[i]&bf.pMem[i];
	return res;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField res (BitLen);
	for (int i =0; i<MemLen-1; i++)
		res.pMem[i]=~pMem[i];
		int index = MemLen-1;
	for (int i=((MemLen-1)*8*sizeof(TELEM)); i<BitLen;i++)
	{
		TELEM mask = GetMemMask(i);
		pMem[index]^=mask;
	}
	return res;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	int bit;
	for (int i=0; i<bf.MemLen; i++)
		bf.pMem[i]=0;
	for (int i=0; i<bf.BitLen; i++)
	{
		istr>>bit;
		if (bit==1)
			bf.SetBit(i);
	}
	istr>>endl;
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i=0; i<bf.BitLen; i++)
	{
		ostr<<bf.GetBit(i);
		if ((i+1)%(8*sizeof(TELEM))==0)
			ostr<<endl;
	}
	ostr<<endl;
	return ostr;
}
