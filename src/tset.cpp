// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля

#include "tset.h"

TSet::TSet(int mp) 
{
	MaxPower=mp;
	BitField(mp);
}

// конструктор копирования
TSet::TSet(const TSet &s) 
{
	MaxPower = s.MaxPower;
	BitField(s.BitField);
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf)
{
	MaxPower = bf.BitLen;
	BitField(bf);
}

TSet::operator TBitField()
{
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	//if (Elem<MaxPower)
	return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	//if (Elem<MaxPower)
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	//if (Elem<MaxPower)
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	if(*this==&s)
		return *this;
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if(MaxPower!=s.MaxPower)
		return 0;
	if (Bitfield!=s.BitField)
		return 0;
	else return 1;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	if(MaxPower!=s.MaxPower)
		return 1;
	if (Bitfield!=s.BitField)
		return 1;
	else return 0;
}

TSet TSet::operator+(const TSet &s) // объединение
{
	//if (MaxPower!=s.MaxPower)
	TSet res(MaxPower);
	res.BitField = Bitfield|s.BitField;
	return res;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	//if (Elem<MaxPower)
	BitField.SetBit(Elem);
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	BitField.ClrBit(Elem);
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	//if (MaxPower!=s.MaxPower)
	TSet res(MaxPower);
	res.BitField = Bitfield*s.BitField;
	return res;
}

TSet TSet::operator~(void) // дополнение
{	
	TSet res(MaxPower);
	res.BitField = ~BitField;
	return res;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	istr>>s.MaxPower;
	istr>>endl;
	delete[]s.BitField;
	s.BitField(MaxPower);
	istr>>s.BitField;
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	ostr<<s.MaxPower<<endl;
	ostr<<s.Bitfield<<endl;
	return ostr;
}
